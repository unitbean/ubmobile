package com.ub.ubmobile.ubMobileNotificationAndroid;

import com.ub.ubmobile.ubMobileDevice.models.DeviceType;
import com.ub.ubmobile.ubMobileDevice.models.UbMobileDeviceDoc;
import com.ub.ubmobile.ubMobileDevice.services.UbMobileDeviceService;
import com.ub.ubmobile.ubMobileNotificationAndroid.models.UbMobileDeviceNotificationAndroidDoc;
import com.ub.ubmobile.ubMobileNotificationAndroid.services.AndroidNotificationService;
import com.ub.ubmobile.ubMobileNotificationAndroid.services.UbMobileDeviceNotificationAndroidService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/mvc-dispatcher-servlet.xml")
public class UbMobileNotificationAndroidTest {

    @Autowired private AndroidNotificationService androidNotificationService;
    @Autowired private UbMobileDeviceService ubMobileDeviceService;
    @Autowired private UbMobileDeviceNotificationAndroidService ubMobileDeviceNotificationAndroidService;

    private UbMobileDeviceDoc testMobile;

    @Before
    public void init() {
        //"private_key_id": "9a0ce917a68d49198cec421338c5b0ac743a80df",
        //"private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCl+GB/mm2akrkH\nEzqCtz37xdRi6yrtZEnIVoN5uzzGJa4HG9pnITNwCO3DijSr30Jh+Ui89XPtcX1j\nwioLpL002cm+HBukGdHhGOzgbO18/twNJMnFXC/ipkaO2UO0iry2GCDsAyRc7+zr\n+w7/RYh0PiGNp21T/fuM1u78JtJA0UYQApxf1J/gVaBiHRXuQAN9oA/LTMVCX5MW\nhT2fLAhzDEv69E/YaL9JxjyE7etSJT30KZA4obAv9RdU0Ob2bplnhg+fVTmaxZ+L\nPm8BbwlIz5xsQhycBTayBfO31oH0FLyOseQCXlK/c0AJCEqdVPyNSnNBLLtVldRB\nU5VNnhqLAgMBAAECggEAJj9yBZYSEiAj8cR1pHAUIbR6eH1XfDOzaS6+x8vleohu\nbYb2svOMxhHYsUplTwJ5atim8ZzjJWIVEmtJgztouwhBnMUffZbRrj0vQZz4seiN\nz52dvhS7mFDXS8438yBR0ukGlT04IQ7Nm6hU0XE1vAwhcWKXKFcMpG1Xz9Ymd/cj\nKG7972FyXT77rOV2P1ogRrIiBo/HzoafH9IVBIud1Hhvq7qo3emASiwmdLEsV3Lm\nnxK6Mcm6ybsBLNfjZnENKMtQXfSB3glcH5JjSunk+54Vb9ak6kJ9LIrbpAfLczDr\nOLI7IIfG7a0Xxn6xNsVLraBXLK+hOaFt1GyR1FQk2QKBgQDb+qCYIgOoRECuNtfW\nSF9vkKUMsUdHru+0xypmejPsvJIKYWdUuzXIIuMl0OwHlQ//+7MR/d4Whx8YJLW+\n5umw256feiPQQ2nJRNfp5TjcDKIQoS2rBaZDb9tmXGjjnqPQAqjee+Zl8yxERWNh\nq/fcfw3F2HU0Hp8Gf4uIeqTM3QKBgQDBJbzDGGIzlcDkEYVNIGeKzznRTpIR4rkd\n0K6T11fWSHLlvlPDfZJ1uaKVDZzh4Wc/Dkn1zhSHLQjdv2K7viZzhkBoRpqopCxs\npF03cF3Xk5+NfVgyuG8hYS9HOAL+PRghqKO/JtqEV7oRc1wYs2s4YW28HxaWOm2E\nAkDmuLw6hwKBgAQBHvVGAT0J6QeC1hD/hkU4o9iETaDfbVukGIB/DvzrPdlUhpYD\nRmpbQDlLAVDuBLmLMmpMbcOJOCavXGOaPnzKsquChK5g28cfKrMGSi+vRy/4TyWC\n19D4c0z8t6gVFjROKBbMuJf4gkDH5pQ7lip8YZrNYuzM9eYfYkHcwa09AoGAY9r4\nvfxrOLQhZ0fPnNI19cOw5o8yYWDPx6RNvU1Fshj+ndxKPfU7tPVmIp1JEFaSJD1k\n61nBrykPPmVlzB5AjtVHkyF9b9aAySIotzK2sW82u9WjtYU4nw85fB5vHbXENhKT\npY2Wx3j7T25OnM6Vji6ApxODf+NxHyBzIVU8qx0CgYAlvNEp1jffbUME8+IcDCeD\nFW7u525ewZntQjWV5mo+/jJSUp1TPh1IOmn+DgfYHOwv4d+vaqcQdaz6ravUSswD\nxaqpbMw7rgy+FzDI6KB9j+OW2fINDk6tSwtfrZ+lZg3Aun99r8bjBYugXo33srd0\nIcWMZizSQGcprBZsUXuTrw\u003d\u003d\n-----END PRIVATE KEY-----\n",
        //"client_email": "276759475910-dfm8i4avjetudbktuouf7dhj9a3fk193@developer.gserviceaccount.com",
        //"client_id": "276759475910-dfm8i4avjetudbktuouf7dhj9a3fk193.apps.googleusercontent.com",
        //"type": "service_account"

        UbMobileDeviceNotificationAndroidDoc ubMobileDeviceNotificationAndroidDoc = new UbMobileDeviceNotificationAndroidDoc();
        //ubMobileDeviceNotificationAndroidDoc.setApiKey("-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCl+GB/mm2akrkH\nEzqCtz37xdRi6yrtZEnIVoN5uzzGJa4HG9pnITNwCO3DijSr30Jh+Ui89XPtcX1j\nwioLpL002cm+HBukGdHhGOzgbO18/twNJMnFXC/ipkaO2UO0iry2GCDsAyRc7+zr\n+w7/RYh0PiGNp21T/fuM1u78JtJA0UYQApxf1J/gVaBiHRXuQAN9oA/LTMVCX5MW\nhT2fLAhzDEv69E/YaL9JxjyE7etSJT30KZA4obAv9RdU0Ob2bplnhg+fVTmaxZ+L\nPm8BbwlIz5xsQhycBTayBfO31oH0FLyOseQCXlK/c0AJCEqdVPyNSnNBLLtVldRB\nU5VNnhqLAgMBAAECggEAJj9yBZYSEiAj8cR1pHAUIbR6eH1XfDOzaS6+x8vleohu\nbYb2svOMxhHYsUplTwJ5atim8ZzjJWIVEmtJgztouwhBnMUffZbRrj0vQZz4seiN\nz52dvhS7mFDXS8438yBR0ukGlT04IQ7Nm6hU0XE1vAwhcWKXKFcMpG1Xz9Ymd/cj\nKG7972FyXT77rOV2P1ogRrIiBo/HzoafH9IVBIud1Hhvq7qo3emASiwmdLEsV3Lm\nnxK6Mcm6ybsBLNfjZnENKMtQXfSB3glcH5JjSunk+54Vb9ak6kJ9LIrbpAfLczDr\nOLI7IIfG7a0Xxn6xNsVLraBXLK+hOaFt1GyR1FQk2QKBgQDb+qCYIgOoRECuNtfW\nSF9vkKUMsUdHru+0xypmejPsvJIKYWdUuzXIIuMl0OwHlQ//+7MR/d4Whx8YJLW+\n5umw256feiPQQ2nJRNfp5TjcDKIQoS2rBaZDb9tmXGjjnqPQAqjee+Zl8yxERWNh\nq/fcfw3F2HU0Hp8Gf4uIeqTM3QKBgQDBJbzDGGIzlcDkEYVNIGeKzznRTpIR4rkd\n0K6T11fWSHLlvlPDfZJ1uaKVDZzh4Wc/Dkn1zhSHLQjdv2K7viZzhkBoRpqopCxs\npF03cF3Xk5+NfVgyuG8hYS9HOAL+PRghqKO/JtqEV7oRc1wYs2s4YW28HxaWOm2E\nAkDmuLw6hwKBgAQBHvVGAT0J6QeC1hD/hkU4o9iETaDfbVukGIB/DvzrPdlUhpYD\nRmpbQDlLAVDuBLmLMmpMbcOJOCavXGOaPnzKsquChK5g28cfKrMGSi+vRy/4TyWC\n19D4c0z8t6gVFjROKBbMuJf4gkDH5pQ7lip8YZrNYuzM9eYfYkHcwa09AoGAY9r4\nvfxrOLQhZ0fPnNI19cOw5o8yYWDPx6RNvU1Fshj+ndxKPfU7tPVmIp1JEFaSJD1k\n61nBrykPPmVlzB5AjtVHkyF9b9aAySIotzK2sW82u9WjtYU4nw85fB5vHbXENhKT\npY2Wx3j7T25OnM6Vji6ApxODf+NxHyBzIVU8qx0CgYAlvNEp1jffbUME8+IcDCeD\nFW7u525ewZntQjWV5mo+/jJSUp1TPh1IOmn+DgfYHOwv4d+vaqcQdaz6ravUSswD\nxaqpbMw7rgy+FzDI6KB9j+OW2fINDk6tSwtfrZ+lZg3Aun99r8bjBYugXo33srd0\nIcWMZizSQGcprBZsUXuTrw\u003d\u003d\n-----END PRIVATE KEY-----\n");
//        ubMobileDeviceNotificationAndroidDoc.setApiKey("9a0ce917a68d49198cec421338c5b0ac743a80df");
//        ubMobileDeviceNotificationAndroidDoc.setApiKey("AIzaSyAtE3KnUP-JKFBgNi8mqAzqej-7YiXFHFI");
        ubMobileDeviceNotificationAndroidDoc.setApiKey("AIzaSyCuAb3TqVwFP_2DiCsEecLmCJYi8GA7uV0");
//        ubMobileDeviceNotificationAndroidDoc.setGcmRegId("276759475910-dfm8i4avjetudbktuouf7dhj9a3fk193.apps.googleusercontent.com");
        ubMobileDeviceNotificationAndroidService.save(ubMobileDeviceNotificationAndroidDoc);

//        String deviceToken = "8795e107-6d2b-3786-9d4f-0a5057184b2c";
        String deviceToken = "dakwzi4dsYs:APA91bE6YtgYTSBhtqa13DFelZCJz_bkHncsYUc7g8MlTqZ3yZqwzdE7uxOX-gfzpfzR2mIwqGbcGQVnX4wNhJvCz3pqjiiymi0DFA6Hs2u6fbnvbLN5vmgDPGFQYHVqo0mJy7FjA3Rm";
        testMobile = ubMobileDeviceService.create(deviceToken, DeviceType.ANDROID);
    }

    @After
    public void after() {
        ubMobileDeviceService.remove(testMobile.getId());
    }

    @Test
    public void simpleTest() throws Exception{
        androidNotificationService.sendAndroidAlert(testMobile,"{\"isJson\":true}");
    }

}
