package com.ub.ubmobile.ubMobileNotificationAndroid.models;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document
public class UbMobileDeviceNotificationAndroidDoc {
    @Id
    private final String id = UbMobileDeviceNotificationAndroidDoc.class.getCanonicalName();
    private String gcmRegId = "";
    private String apiKey = "";
    private AndroidPriorityEnum priority = AndroidPriorityEnum.NORMAL;
    private Boolean delayWhileIdle = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {

    }

    public String getGcmRegId() {
        return gcmRegId;
    }

    public void setGcmRegId(String gcmRegId) {
        this.gcmRegId = gcmRegId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public AndroidPriorityEnum getPriority() {
        return priority;
    }

    public void setPriority(AndroidPriorityEnum priority) {
        this.priority = priority;
    }

    public Boolean getDelayWhileIdle() {
        return delayWhileIdle;
    }

    public void setDelayWhileIdle(Boolean delayWhileIdle) {
        this.delayWhileIdle = delayWhileIdle;
    }
}
