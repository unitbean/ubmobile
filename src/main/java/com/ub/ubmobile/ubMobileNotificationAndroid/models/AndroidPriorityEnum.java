package com.ub.ubmobile.ubMobileNotificationAndroid.models;

/**
 * Created by kornev on 01/08/16.
 */
public enum AndroidPriorityEnum {
    NORMAL("Normal"), HIGH("High");

    private String title;

    AndroidPriorityEnum(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
