package com.ub.ubmobile.ubMobileNotificationAndroid.controllers;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.base.views.breadcrumbs.BreadcrumbsLink;
import com.ub.core.base.views.pageHeader.PageHeader;
import com.ub.ubmobile.ubMobileDevice.models.DeviceType;
import com.ub.ubmobile.ubMobileDevice.services.UbMobileDeviceService;
import com.ub.ubmobile.ubMobileNotificationAndroid.models.AndroidPriorityEnum;
import com.ub.ubmobile.ubMobileNotificationAndroid.routes.UbMobileDeviceNotificationAndroidAdminRoutes;
import com.ub.ubmobile.ubMobileNotificationAndroid.services.AndroidNotificationService;
import com.ub.ubmobile.ubMobileNotificationAndroid.services.UbMobileDeviceNotificationAndroidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UbMobileDeviceNotificationAndroidAdminController {
    @Autowired private UbMobileDeviceNotificationAndroidService ubMobileNotificationAndroidService;
    @Autowired private AndroidNotificationService androidNotificationService;
    @Autowired private UbMobileDeviceService ubMobileDeviceService;

    private PageHeader defaultPageHeader(String current){
        PageHeader pageHeader = PageHeader.defaultPageHeader();
        pageHeader.setLinkAdd(UbMobileDeviceNotificationAndroidAdminRoutes.ADD);
        pageHeader.getBreadcrumbs().getLinks().add(new BreadcrumbsLink(UbMobileDeviceNotificationAndroidAdminRoutes.ALL,"Все Notification Android"));
        pageHeader.getBreadcrumbs().setCurrentPageTitle(current);
        return pageHeader;
    }

//    @RequestMapping(value = UbMobileDeviceNotificationAndroidAdminRoutes.ALL, method = RequestMethod.GET)
//    public String all(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
//                      @RequestParam(required = false, defaultValue = "") String query,
//                      Model model) {
//        SearchUbMobileDeviceNotificationAndroidAdminRequest searchUbMobileDeviceNotificationAndroidAdminRequest = new SearchUbMobileDeviceNotificationAndroidAdminRequest(currentPage);
//        searchUbMobileDeviceNotificationAndroidAdminRequest.setQuery(query);
//        List<UbMobileDeviceNotificationAndroidDoc> ubMobileDeviceNotificationAndroidDocs = new ArrayList<UbMobileDeviceNotificationAndroidDoc>();
//        ubMobileDeviceNotificationAndroidDocs.add(ubMobileNotificationAndroidService.getUbMobileDeviceNotificationAndroidDoc());
//        model.addAttribute("searchUbMobileDeviceNotificationAndroidAdminResponse", ubMobileDeviceNotificationAndroidDocs);
//        model.addAttribute("pageHeader", defaultPageHeader("Все"));
//        return "com.ub.ubmobile.admin.ubMobileNotificationAndroid.all";
//    }

    @RequestMapping(value = UbMobileDeviceNotificationAndroidAdminRoutes.EDIT, method = RequestMethod.GET)
    public String edit(Model model) {
        model.addAttribute("ubMobileNotificationAndroidDoc", ubMobileNotificationAndroidService.getUbMobileDeviceNotificationAndroidDoc());
        model.addAttribute("pageHeader", defaultPageHeader("Редактирование"));

        return "com.ub.ubmobile.admin.ubMobileNotificationAndroid.edit";
    }

    @RequestMapping(value = UbMobileDeviceNotificationAndroidAdminRoutes.EDIT, method = RequestMethod.POST)
    public String edit(@RequestParam String apiKey, @RequestParam String gcmRegId,
                       @RequestParam AndroidPriorityEnum priority,
                       @RequestParam(required = false, defaultValue = "false") Boolean delayWhileIdle,
                       Model model) {
        model.addAttribute("ubMobileNotificationAndroidDoc", ubMobileNotificationAndroidService.update(apiKey, gcmRegId, priority, delayWhileIdle));
        model.addAttribute("pageHeader", defaultPageHeader("Редактирование"));
        return "com.ub.ubmobile.admin.ubMobileNotificationAndroid.edit";
    }

    @RequestMapping(value = UbMobileDeviceNotificationAndroidAdminRoutes.TEST, method = RequestMethod.GET)
    public String test(@RequestParam String message, @RequestParam String token) {

        androidNotificationService.sendAndroidAlert(ubMobileDeviceService.findByDeviceToken(token, DeviceType.ANDROID), message);
        return RouteUtils.redirectTo("/admin");
    }
}
