package com.ub.ubmobile.ubMobileNotificationAndroid.menu;

import com.ub.core.base.menu.CoreMenu;
import com.ub.core.menu.models.fields.MenuIcons;

public class UbMobileDeviceNotificationAndroidMenu extends CoreMenu{
    public UbMobileDeviceNotificationAndroidMenu() {
        this.name = "Notification Android";
        this.icon = MenuIcons.ENTYPO_DOC_TEXT;
    }
}
