package com.ub.ubmobile.ubMobileNotificationAndroid.menu;

import com.ub.ubmobile.ubMobileNotificationAndroid.routes.UbMobileDeviceNotificationAndroidAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class UbMobileDeviceNotificationAndroidAllMenu extends CoreMenu {
    public UbMobileDeviceNotificationAndroidAllMenu() {
        this.name ="Все";
        this.parent = new UbMobileDeviceNotificationAndroidMenu();
        this.url = UbMobileDeviceNotificationAndroidAdminRoutes.ALL;
    }
}
