package com.ub.ubmobile.ubMobileNotificationAndroid.menu;

import com.ub.ubmobile.ubMobileNotificationAndroid.routes.UbMobileDeviceNotificationAndroidAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class UbMobileDeviceNotificationAndroidAddMenu extends CoreMenu {
    public UbMobileDeviceNotificationAndroidAddMenu() {
        this.name ="Добавить";
        this.parent = new UbMobileDeviceNotificationAndroidMenu();
        this.url = UbMobileDeviceNotificationAndroidAdminRoutes.ADD;
    }
}
