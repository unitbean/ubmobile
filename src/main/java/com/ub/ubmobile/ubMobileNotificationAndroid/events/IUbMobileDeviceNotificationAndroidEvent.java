package com.ub.ubmobile.ubMobileNotificationAndroid.events;

import com.ub.ubmobile.ubMobileNotificationAndroid.models.UbMobileDeviceNotificationAndroidDoc;

public interface IUbMobileDeviceNotificationAndroidEvent {
    public void preSave(UbMobileDeviceNotificationAndroidDoc ubMobileNotificationAndroidDoc);
    public void afterSave(UbMobileDeviceNotificationAndroidDoc ubMobileNotificationAndroidDoc);

    public Boolean preDelete(UbMobileDeviceNotificationAndroidDoc ubMobileNotificationAndroidDoc);
    public void afterDelete(UbMobileDeviceNotificationAndroidDoc ubMobileNotificationAndroidDoc);
}
