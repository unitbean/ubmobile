package com.ub.ubmobile.ubMobileNotificationAndroid.views.all;

import com.ub.core.base.search.SearchRequest;

public class SearchUbMobileDeviceNotificationAndroidAdminRequest extends SearchRequest{
    public SearchUbMobileDeviceNotificationAndroidAdminRequest() {
    }

    public SearchUbMobileDeviceNotificationAndroidAdminRequest(Integer currentPage){
        this.currentPage = currentPage;
    }

    public SearchUbMobileDeviceNotificationAndroidAdminRequest(Integer currentPage, Integer pageSize){
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

}
