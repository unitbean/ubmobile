package com.ub.ubmobile.ubMobileNotificationAndroid.views.all;

import com.ub.core.base.search.SearchResponse;
import com.ub.ubmobile.ubMobileNotificationAndroid.models.UbMobileDeviceNotificationAndroidDoc;

import java.util.List;

public class SearchUbMobileDeviceNotificationAndroidAdminResponse extends SearchResponse {
    private List<UbMobileDeviceNotificationAndroidDoc> result;


    public SearchUbMobileDeviceNotificationAndroidAdminResponse() {
    }

    public SearchUbMobileDeviceNotificationAndroidAdminResponse(Integer currentPage, Integer pageSize, List<UbMobileDeviceNotificationAndroidDoc> result) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.result = result;
    }

    public List<UbMobileDeviceNotificationAndroidDoc> getResult() {
        return result;
    }

    public void setResult(List<UbMobileDeviceNotificationAndroidDoc> result) {
        this.result = result;
    }
}
