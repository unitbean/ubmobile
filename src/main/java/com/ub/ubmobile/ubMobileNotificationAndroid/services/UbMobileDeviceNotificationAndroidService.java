package com.ub.ubmobile.ubMobileNotificationAndroid.services;

import com.ub.ubmobile.ubMobileNotificationAndroid.models.AndroidPriorityEnum;
import com.ub.ubmobile.ubMobileNotificationAndroid.models.UbMobileDeviceNotificationAndroidDoc;
import com.ub.ubmobile.ubMobileNotificationIos.models.UbMobileDeviceNotificationIosDoc;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class UbMobileDeviceNotificationAndroidService {

    @Autowired private MongoTemplate mongoTemplate;


    public UbMobileDeviceNotificationAndroidDoc save(UbMobileDeviceNotificationAndroidDoc ubMobileNotificationAndroidDoc) {
        mongoTemplate.save(ubMobileNotificationAndroidDoc);
        return ubMobileNotificationAndroidDoc;
    }

    /**
     * Достаем проперти андройд уведомлений, если нету то создаем новые
     * @return
     */
    public UbMobileDeviceNotificationAndroidDoc getUbMobileDeviceNotificationAndroidDoc() {
        Criteria criteria = Criteria.where("_id").is(UbMobileDeviceNotificationAndroidDoc.class.getCanonicalName());
        UbMobileDeviceNotificationAndroidDoc ubMobileDeviceNotificationAndroidDoc =
                mongoTemplate.findOne(
                        new Query(criteria),
                        UbMobileDeviceNotificationAndroidDoc.class);

        if(ubMobileDeviceNotificationAndroidDoc == null){
            ubMobileDeviceNotificationAndroidDoc = new UbMobileDeviceNotificationAndroidDoc();
            save(ubMobileDeviceNotificationAndroidDoc);
        }

        return ubMobileDeviceNotificationAndroidDoc;
    }

    /**
     * update properties
     * @param apiKey
     * @param gcmRegId
     * @return
     */
    public UbMobileDeviceNotificationAndroidDoc update(String apiKey, String gcmRegId, AndroidPriorityEnum priority, Boolean delayWhileIdle) {
        UbMobileDeviceNotificationAndroidDoc properties = getUbMobileDeviceNotificationAndroidDoc();

        properties.setApiKey(apiKey);
        properties.setGcmRegId(gcmRegId);
        properties.setPriority(priority);
        properties.setDelayWhileIdle(delayWhileIdle);

        mongoTemplate.save(properties);
        return properties;
    }

}
