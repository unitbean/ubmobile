package com.ub.ubmobile.ubMobileNotificationAndroid.services;

import com.ub.ubmobile.ubMobileDevice.models.UbMobileDeviceDoc;
import com.ub.ubmobile.ubMobileNotificationAndroid.models.AndroidPriorityEnum;
import com.ub.ubmobile.ubMobileNotificationAndroid.utils.Message;
import com.ub.ubmobile.ubMobileNotificationAndroid.utils.MulticastResult;
import com.ub.ubmobile.ubMobileNotificationAndroid.utils.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AndroidNotificationService {

    @Autowired private UbMobileDeviceNotificationAndroidService ubMobileDeviceNotificationAndroidService;

    public void sendAndroidAlert(UbMobileDeviceDoc ubMobileDeviceDoc, String message) {
        List<UbMobileDeviceDoc> ids = new ArrayList<UbMobileDeviceDoc>();
        ids.add(ubMobileDeviceDoc);
        sendAndroidAlert(ids, message);
    }

    public void sendAndroidAlert(List<UbMobileDeviceDoc> ubMobileDeviceDocs, String message) {
        try {
            String apiKey = ubMobileDeviceNotificationAndroidService.getUbMobileDeviceNotificationAndroidDoc().getApiKey();
            Sender sender = new Sender(apiKey);
            Message objMessage = new Message.Builder().timeToLive(30)
                    .isHighPriority(ubMobileDeviceNotificationAndroidService.getUbMobileDeviceNotificationAndroidDoc().getPriority().equals(AndroidPriorityEnum.HIGH))
                    .delayWhileIdle(ubMobileDeviceNotificationAndroidService.getUbMobileDeviceNotificationAndroidDoc().getDelayWhileIdle())
                    .addData("message", message).build();

            List<String> registrationIDs = new ArrayList<String>();
            for (UbMobileDeviceDoc ubMobileDeviceDoc : ubMobileDeviceDocs) {
                if (ubMobileDeviceDoc.getDeviceToken() == null || ubMobileDeviceDoc.getDeviceToken().equals(""))
                    continue;
                registrationIDs.add(ubMobileDeviceDoc.getDeviceToken());
            }

            if(registrationIDs.size() == 0) return;
            MulticastResult result = sender.send(objMessage, registrationIDs, 1);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (java.lang.IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public void sendAndroidAlert(List<String> registrationIDs, Map<String, String> customData) throws IOException {

        String apiKey = ubMobileDeviceNotificationAndroidService.getUbMobileDeviceNotificationAndroidDoc().getApiKey();
        Sender sender = new Sender(apiKey);
        Message.Builder messageBuilder = new Message.Builder().timeToLive(30)
                .isHighPriority(ubMobileDeviceNotificationAndroidService.getUbMobileDeviceNotificationAndroidDoc().getPriority().equals(AndroidPriorityEnum.HIGH))
                .delayWhileIdle(ubMobileDeviceNotificationAndroidService.getUbMobileDeviceNotificationAndroidDoc().getDelayWhileIdle());

        for(Map.Entry<String, String> data : customData.entrySet()) {
            messageBuilder.addData(data.getKey(), data.getValue());
        }

        MulticastResult result = sender.send(messageBuilder.build(), registrationIDs, 1);
    }
}
