package com.ub.ubmobile.ubMobileNotificationIos.views.all;

import com.ub.core.base.search.SearchRequest;

public class SearchUbMobileDeviceNotificationIosAdminRequest extends SearchRequest{
    public SearchUbMobileDeviceNotificationIosAdminRequest() {
    }

    public SearchUbMobileDeviceNotificationIosAdminRequest(Integer currentPage){
        this.currentPage = currentPage;
    }

    public SearchUbMobileDeviceNotificationIosAdminRequest(Integer currentPage, Integer pageSize){
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

}
