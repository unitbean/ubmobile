package com.ub.ubmobile.ubMobileNotificationIos.views.all;

import com.ub.core.base.search.SearchResponse;
import com.ub.ubmobile.ubMobileNotificationIos.models.UbMobileDeviceNotificationIosDoc;

import java.util.List;

public class SearchUbMobileDeviceNotificationIosAdminResponse extends SearchResponse {
    private List<UbMobileDeviceNotificationIosDoc> result;


    public SearchUbMobileDeviceNotificationIosAdminResponse() {
    }

    public SearchUbMobileDeviceNotificationIosAdminResponse(Integer currentPage, Integer pageSize, List<UbMobileDeviceNotificationIosDoc> result) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.result = result;
    }

    public List<UbMobileDeviceNotificationIosDoc> getResult() {
        return result;
    }

    public void setResult(List<UbMobileDeviceNotificationIosDoc> result) {
        this.result = result;
    }
}
