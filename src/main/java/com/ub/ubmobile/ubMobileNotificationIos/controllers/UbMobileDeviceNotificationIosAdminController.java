package com.ub.ubmobile.ubMobileNotificationIos.controllers;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.exceptions.NetworkIOException;
import com.ub.core.base.views.pageHeader.PageHeader;
import com.ub.ubmobile.ubMobileNotificationIos.models.IosNotificationType;
import com.ub.ubmobile.ubMobileNotificationIos.models.UbMobileDeviceNotificationIosDoc;
import com.ub.ubmobile.ubMobileNotificationIos.routes.UbMobileDeviceNotificationIosAdminRoutes;
import com.ub.ubmobile.ubMobileNotificationIos.services.UbMobileDeviceNotificationIosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Controller
public class UbMobileDeviceNotificationIosAdminController {
    @Autowired private UbMobileDeviceNotificationIosService ubMobileNotificationIosService;

    private PageHeader defaultPageHeader(String current) {
        PageHeader pageHeader = PageHeader.defaultPageHeader();
        pageHeader.getBreadcrumbs().setCurrentPageTitle(current);
        return pageHeader;
    }

//    @RequestMapping(value = UbMobileDeviceNotificationIosAdminRoutes.ALL, method = RequestMethod.GET)
//    public String all(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
//                      @RequestParam(required = false, defaultValue = "") String query,
//                      Model model) {
//        SearchUbMobileDeviceNotificationIosAdminRequest searchUbMobileDeviceNotificationIosAdminRequest = new SearchUbMobileDeviceNotificationIosAdminRequest(currentPage);
//        searchUbMobileDeviceNotificationIosAdminRequest.setQuery(query);
//        model.addAttribute("searchUbMobileDeviceNotificationIosAdminResponse", ubMobileNotificationIosService.findAll(searchUbMobileDeviceNotificationIosAdminRequest));
//        model.addAttribute("pageHeader",defaultPageHeader("Все"));
//        return "com.ub.ubmobile.admin.ubMobileNotificationIos.all";
//    }

//    @RequestMapping(value = UbMobileDeviceNotificationIosAdminRoutes.MODAL_PARENT, method = RequestMethod.GET)
//    public String modalResponse(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
//                                @RequestParam(required = false, defaultValue = "") String query,
//                                Model model) {
//        SearchUbMobileDeviceNotificationIosAdminRequest searchUbMobileDeviceNotificationIosAdminRequest = new SearchUbMobileDeviceNotificationIosAdminRequest(currentPage);
//        searchUbMobileDeviceNotificationIosAdminRequest.setQuery(query);
//        model.addAttribute("searchUbMobileDeviceNotificationIosAdminResponse", ubMobileNotificationIosService.findAll(searchUbMobileDeviceNotificationIosAdminRequest));
//        return "com.ub.ubmobile.admin.ubMobileNotificationIos.modal.parent";
//    }

    @RequestMapping(value = UbMobileDeviceNotificationIosAdminRoutes.EDIT, method = RequestMethod.GET)
    public String edit(Model model) {
        model.addAttribute("ubMobileNotificationIosDoc", ubMobileNotificationIosService.findById());
        model.addAttribute("pageHeader", defaultPageHeader("Редактирование"));
        return "com.ub.ubmobile.admin.ubMobileNotificationIos.edit";
    }

    @RequestMapping(value = UbMobileDeviceNotificationIosAdminRoutes.EDIT, method = RequestMethod.POST)
    public String edit(@RequestParam String password, @RequestParam MultipartFile file,
                       @RequestParam IosNotificationType type, Model model) {
        model.addAttribute("ubMobileNotificationIosDoc", ubMobileNotificationIosService.update(password, file, type));
        model.addAttribute("pageHeader", defaultPageHeader("Редактирование"));
        return "com.ub.ubmobile.admin.ubMobileNotificationIos.edit";
    }

    @ResponseBody
    @RequestMapping(value = "/admin/ios/sendtest", method = RequestMethod.GET)
    public String sendTestPush() {
        try {

            List<String> token = new ArrayList<String>();
            token.add("8a0077b82d60b06d68410ed385e8c87cf77f34e4281ebb37ebd90b838874ae22");
            token.add("8a0077b82d60b06d68410ed385e8c87cf77f34e4281ebb37ebd90b838874ae22");
            token.add("da29897506d3b7ef1b6ac9ab18138e87dd1ed180632486d613f040026824df36");
            token.add("da29897506d3b7ef1b6ac9ab18138e87dd1ed180632486d613f040026824df36");
            token.add("7fed23a513b16a8c117ccaf7325a4f6ecd97b98b03fe270bd1bfa675c930fa8a");
            token.add("eaf2b82535dcfdf554c661c23e552dd48648ad3a3e225b0c409a4e9c2c8b06e9");
            UbMobileDeviceNotificationIosDoc properties = ubMobileNotificationIosService.findById();

            ApnsService service = APNS.newService().withCert(ubMobileNotificationIosService.getCertificate(), properties.getPassword()).withSandboxDestination().build();

            String payload = APNS.newPayload().badge(1).alertBody("{\"message\":\"asdkfj sdkf\"}").build();
            service.push(token, payload);
            service.push("eaf2b82535dcfdf554c661c23e552dd48648ad3a3e225b0c409a4e9c2c8b06e9", payload);
        } catch (NetworkIOException e) {
            return "bad";
        } catch (Exception e) {
            return "bad";
        }

        return "pk";
    }

//    @RequestMapping(value = UbMobileDeviceNotificationIosAdminRoutes.DELETE, method = RequestMethod.GET)
//    public String delete(@RequestParam ObjectId id, Model model) {
//        model.addAttribute("id", id);
//        model.addAttribute("pageHeader",defaultPageHeader("Удаление"));
//        return "com.ub.ubmobile.admin.ubMobileNotificationIos.delete";
//    }
//
//    @RequestMapping(value = UbMobileDeviceNotificationIosAdminRoutes.DELETE, method = RequestMethod.POST)
//    public String delete(@RequestParam ObjectId id) {
//        ubMobileNotificationIosService.remove(id);
//        return RouteUtils.redirectTo(UbMobileDeviceNotificationIosAdminRoutes.ALL);
//    }
}
