package com.ub.ubmobile.ubMobileNotificationIos.services;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;
import com.ub.ubmobile.ubMobileNotificationIos.models.IosNotification;
import com.ub.ubmobile.ubMobileNotificationIos.models.IosNotificationType;
import com.ub.ubmobile.ubMobileNotificationIos.models.UbMobileDeviceNotificationIosDoc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Eduard on 02.12.2015.
 */
@Component
public class IosNotificationService {

    @Autowired private UbMobileDeviceNotificationIosService ubMobileNotificationIosService;

    /**
     * Form apns service
     * @return
     */
    private ApnsService getApnsService() {
        UbMobileDeviceNotificationIosDoc properties = ubMobileNotificationIosService.findById();

        if (IosNotificationType.PRODUCTION.equals(properties.getType())) {
            return APNS.newService().withCert(ubMobileNotificationIosService.getCertificate(), properties.getPassword()).withProductionDestination().build();
        } else {
            return APNS.newService().withCert(ubMobileNotificationIosService.getCertificate(), properties.getPassword()).withSandboxDestination().build();
        }

    }

    /**
     * form and send notification
     */
    public void sendNotifications(IosNotification notification) {
        ApnsService service = getApnsService();
        PayloadBuilder payloadBuilder = APNS.newPayload();

        payloadBuilder = payloadBuilder.badge(notification.getBadge());
        if (notification.getCustomFields().size() > 0) {
            payloadBuilder = payloadBuilder.customFields(notification.getCustomFields());
        }
        if (notification.getMessage() != null) {
            payloadBuilder = payloadBuilder.alertBody(notification.getMessage());
        }
        if (notification.getTitle() != null) {
            payloadBuilder = payloadBuilder.alertTitle(notification.getTitle());
        }

        payloadBuilder = payloadBuilder.sound("notification.caf");
        //        payloadBuilder = payloadBuilder.sound(notification.getSound());

        String payload = payloadBuilder.build();

        service.push(notification.getTokens(), payload);
    }
}
