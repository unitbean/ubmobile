package com.ub.ubmobile.ubMobileNotificationIos.services;

import com.ub.core.file.services.FileService;
import com.ub.ubmobile.ubMobileNotificationIos.models.IosNotificationType;
import com.ub.ubmobile.ubMobileNotificationIos.models.UbMobileDeviceNotificationIosDoc;
import com.ub.ubmobile.ubMobileNotificationIos.events.IUbMobileDeviceNotificationIosEvent;
import  com.ub.ubmobile.ubMobileNotificationIos.views.all.SearchUbMobileDeviceNotificationIosAdminRequest;
import  com.ub.ubmobile.ubMobileNotificationIos.views.all.SearchUbMobileDeviceNotificationIosAdminResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.bson.types.ObjectId;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

@Component
public class UbMobileDeviceNotificationIosService {
    private static Map<String,IUbMobileDeviceNotificationIosEvent> ubMobileNotificationIosEvents = new HashMap<String,IUbMobileDeviceNotificationIosEvent>();

    @Autowired private MongoTemplate mongoTemplate;
    @Autowired private FileService fileService;

    public static void addUbMobileDeviceNotificationIosEvent(IUbMobileDeviceNotificationIosEvent iUbMobileDeviceNotificationIosEvent){
            ubMobileNotificationIosEvents.put(iUbMobileDeviceNotificationIosEvent.getClass().getCanonicalName(), iUbMobileDeviceNotificationIosEvent);
    }

    public UbMobileDeviceNotificationIosDoc save(UbMobileDeviceNotificationIosDoc ubMobileNotificationIosDoc){
            mongoTemplate.save(ubMobileNotificationIosDoc);
            callAfterSave(ubMobileNotificationIosDoc);
            return ubMobileNotificationIosDoc;
    }

    public void remove(ObjectId id){
        UbMobileDeviceNotificationIosDoc ubMobileNotificationIosDoc = findById();
        if (ubMobileNotificationIosDoc == null) return;
        mongoTemplate.remove(ubMobileNotificationIosDoc);
        callAfterDelete(ubMobileNotificationIosDoc);
    }

    /**
     * Get properties
     * @return
     */
    public UbMobileDeviceNotificationIosDoc findById() {
        UbMobileDeviceNotificationIosDoc properties = mongoTemplate.findById(UbMobileDeviceNotificationIosDoc.class.getCanonicalName(), UbMobileDeviceNotificationIosDoc.class);

        if (properties == null) {
            properties = new UbMobileDeviceNotificationIosDoc();
            mongoTemplate.save(properties);
        }

        return properties;
    }

    /**
     * update properties
     * @param password
     * @param file
     * @return
     */
    public UbMobileDeviceNotificationIosDoc update(String password, MultipartFile file, IosNotificationType type) {
        UbMobileDeviceNotificationIosDoc properties = findById();

        properties.setPassword(password);
        properties.setType(type);

        if (file != null) {
            ObjectId cert = fileService.saveWithDelete(file, properties.getCertificate());
            properties.setCertificate(cert);
            properties.setFileName(file.getOriginalFilename());
        }

        mongoTemplate.save(properties);
        return properties;
    }

    /**
     * get input stream from certificate
     * @return
     */
    public InputStream getCertificate() {
        UbMobileDeviceNotificationIosDoc properties = findById();
        return fileService.getFile(properties.getCertificate()).getInputStream();
    }

    private void callAfterSave(UbMobileDeviceNotificationIosDoc ubMobileNotificationIosDoc){
        for(IUbMobileDeviceNotificationIosEvent iUbMobileDeviceNotificationIosEvent : ubMobileNotificationIosEvents.values()){
            iUbMobileDeviceNotificationIosEvent.afterSave(ubMobileNotificationIosDoc);
        }
    }
    private void callAfterDelete(UbMobileDeviceNotificationIosDoc ubMobileNotificationIosDoc){
        for(IUbMobileDeviceNotificationIosEvent iUbMobileDeviceNotificationIosEvent : ubMobileNotificationIosEvents.values()){
            iUbMobileDeviceNotificationIosEvent.afterDelete(ubMobileNotificationIosDoc);
        }
    }
}
