package com.ub.ubmobile.ubMobileNotificationIos.models;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document
public class UbMobileDeviceNotificationIosDoc {
    @Id
    private final String id = UbMobileDeviceNotificationIosDoc.class.getCanonicalName();

    private ObjectId certificate;
    private String fileName = ""; // certificate name
    private String password = "";
    private IosNotificationType type = IosNotificationType.SANDBOX;

    public String getId(){
        return id;
    }
    public void setId(String id){

    }

    public ObjectId getCertificate() {
        return certificate;
    }

    public void setCertificate(ObjectId certificate) {
        this.certificate = certificate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public IosNotificationType getType() {
        return type;
    }

    public void setType(IosNotificationType type) {
        this.type = type;
    }
}
