package com.ub.ubmobile.ubMobileNotificationIos.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * util helped class for build ios notifications
 * Created by Eduard on 02.12.2015.
 */
public class IosNotification {

    public static final String CUSTOM_DATA_FIELDS = "customData";

    private int badge = 1;
    private String sound;
    private String message;
    private String title;

    private List<String> tokens = new ArrayList<String>();
    private Map<String, Object> customFields = new HashMap<String, Object>();

    public IosNotification() {
    }

    public IosNotification(int badge, String sound, String message, String title, List<String> tokens, Map<String, Object> customFields) {
        this.badge = badge;
        this.sound = sound;
        this.message = message;
        this.title = title;
        this.tokens = tokens;
        this.customFields = customFields;
    }

    public int getBadge() {
        return badge;
    }

    public void setBadge(int badge) {
        this.badge = badge;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, Object> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(Map<String, Object> customFields) {
        this.customFields = customFields;
    }
}
