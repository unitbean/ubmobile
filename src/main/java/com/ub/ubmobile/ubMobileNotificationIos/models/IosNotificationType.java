package com.ub.ubmobile.ubMobileNotificationIos.models;

/**
 * Created by kornev on 21/06/16.
 */
public enum IosNotificationType {
    SANDBOX("Sandbox"), PRODUCTION("Production");

    private String title;

    IosNotificationType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
