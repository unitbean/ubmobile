package com.ub.ubmobile.ubMobileNotificationIos.menu;

import com.ub.ubmobile.ubMobileNotificationIos.routes.UbMobileDeviceNotificationIosAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class UbMobileDeviceNotificationIosAllMenu extends CoreMenu {
    public UbMobileDeviceNotificationIosAllMenu() {
        this.name ="Все";
        this.parent = new UbMobileDeviceNotificationIosMenu();
        this.url = UbMobileDeviceNotificationIosAdminRoutes.ALL;
    }
}
