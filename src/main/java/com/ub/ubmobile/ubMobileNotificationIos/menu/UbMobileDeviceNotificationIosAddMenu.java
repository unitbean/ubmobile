package com.ub.ubmobile.ubMobileNotificationIos.menu;

import com.ub.ubmobile.ubMobileNotificationIos.routes.UbMobileDeviceNotificationIosAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class UbMobileDeviceNotificationIosAddMenu extends CoreMenu {
    public UbMobileDeviceNotificationIosAddMenu() {
        this.name ="Добавить";
        this.parent = new UbMobileDeviceNotificationIosMenu();
        this.url = UbMobileDeviceNotificationIosAdminRoutes.ADD;
    }
}
