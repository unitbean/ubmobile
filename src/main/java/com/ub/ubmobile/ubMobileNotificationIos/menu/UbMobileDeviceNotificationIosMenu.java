package com.ub.ubmobile.ubMobileNotificationIos.menu;

import com.ub.core.base.menu.CoreMenu;
import com.ub.core.menu.models.fields.MenuIcons;

public class UbMobileDeviceNotificationIosMenu extends CoreMenu{
    public UbMobileDeviceNotificationIosMenu() {
        this.name = "Notification Ios";
        this.icon = MenuIcons.ENTYPO_DOC_TEXT;
    }
}
