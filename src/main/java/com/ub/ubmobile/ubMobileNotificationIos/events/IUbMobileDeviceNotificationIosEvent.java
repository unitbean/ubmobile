package com.ub.ubmobile.ubMobileNotificationIos.events;

import com.ub.ubmobile.ubMobileNotificationIos.models.UbMobileDeviceNotificationIosDoc;

public interface IUbMobileDeviceNotificationIosEvent {
    public void preSave(UbMobileDeviceNotificationIosDoc ubMobileNotificationIosDoc);
    public void afterSave(UbMobileDeviceNotificationIosDoc ubMobileNotificationIosDoc);

    public Boolean preDelete(UbMobileDeviceNotificationIosDoc ubMobileNotificationIosDoc);
    public void afterDelete(UbMobileDeviceNotificationIosDoc ubMobileNotificationIosDoc);
}
