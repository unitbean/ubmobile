package com.ub.ubmobile.ubMobileDevice.menu;

import com.ub.ubmobile.ubMobileDevice.routes.UbMobileDeviceAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class UbMobileDeviceAllMenu extends CoreMenu {
    public UbMobileDeviceAllMenu() {
        this.name ="Все";
        this.parent = new UbMobileDeviceMenu();
        this.url = UbMobileDeviceAdminRoutes.ALL;
    }
}
