package com.ub.ubmobile.ubMobileDevice.menu;

import com.ub.ubmobile.ubMobileDevice.routes.UbMobileDeviceAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class UbMobileDeviceAddMenu extends CoreMenu {
    public UbMobileDeviceAddMenu() {
        this.name ="Добавить";
        this.parent = new UbMobileDeviceMenu();
        this.url = UbMobileDeviceAdminRoutes.ADD;
    }
}
