package com.ub.ubmobile.ubMobileDevice.menu;

import com.ub.core.base.menu.CoreMenu;
import com.ub.core.menu.models.fields.MenuIcons;

public class UbMobileDeviceMenu extends CoreMenu{
    public UbMobileDeviceMenu() {
        this.name = "Mobile Device";
        this.icon = MenuIcons.ENTYPO_DOC_TEXT;
    }
}
