package com.ub.ubmobile.ubMobileDevice.events;

import com.ub.ubmobile.ubMobileDevice.models.UbMobileDeviceDoc;

public interface IUbMobileDeviceEvent {
    public void preSave(UbMobileDeviceDoc ubMobileDeviceDoc);
    public void afterSave(UbMobileDeviceDoc ubMobileDeviceDoc);

    public Boolean preDelete(UbMobileDeviceDoc ubMobileDeviceDoc);
    public void afterDelete(UbMobileDeviceDoc ubMobileDeviceDoc);
}
