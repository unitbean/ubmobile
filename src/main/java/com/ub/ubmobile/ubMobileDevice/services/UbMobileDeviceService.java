package com.ub.ubmobile.ubMobileDevice.services;

import com.ub.core.user.models.UserDoc;
import com.ub.ubmobile.ubMobileDevice.models.DeviceType;
import com.ub.ubmobile.ubMobileDevice.models.UbMobileDeviceDoc;
import com.ub.ubmobile.ubMobileDevice.events.IUbMobileDeviceEvent;
import com.ub.ubmobile.ubMobileDevice.views.all.SearchUbMobileDeviceAdminRequest;
import com.ub.ubmobile.ubMobileDevice.views.all.SearchUbMobileDeviceAdminResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.bson.types.ObjectId;

import java.util.*;

@Component
public class UbMobileDeviceService {
    private static Map<String, IUbMobileDeviceEvent> ubMobileDeviceEvents = new HashMap<String, IUbMobileDeviceEvent>();

    @Autowired private MongoTemplate mongoTemplate;

    public static void addUbMobileDeviceEvent(IUbMobileDeviceEvent iUbMobileDeviceEvent) {
        ubMobileDeviceEvents.put(iUbMobileDeviceEvent.getClass().getCanonicalName(), iUbMobileDeviceEvent);
    }

    public UbMobileDeviceDoc create(String deviceToken, DeviceType deviceType, ObjectId userId) {
        UbMobileDeviceDoc ubMobileDeviceDoc = create(deviceToken, deviceType);
        ubMobileDeviceDoc.setUserId(userId);
        return save(ubMobileDeviceDoc);
    }

    public UbMobileDeviceDoc create(String deviceToken, DeviceType deviceType) {
        UbMobileDeviceDoc ubMobileDeviceDoc = findByDeviceToken(deviceToken, deviceType);
        if (ubMobileDeviceDoc != null) return ubMobileDeviceDoc;

        ubMobileDeviceDoc = new UbMobileDeviceDoc();
        ubMobileDeviceDoc.setDeviceToken(deviceToken);
        ubMobileDeviceDoc.setDeviceType(deviceType);

        return save(ubMobileDeviceDoc);
    }

    public List<UbMobileDeviceDoc> findByUser(UserDoc userDoc) {
        return mongoTemplate.find(
                new Query(Criteria.where("userId").is(userDoc.getId())),
                UbMobileDeviceDoc.class);
    }

    public List<UbMobileDeviceDoc> findByUsers(List<UserDoc> userDocs) {
        List<ObjectId> ids = new ArrayList<ObjectId>();
        for (UserDoc userDoc : userDocs) {
            ids.add(userDoc.getId());
        }
        return mongoTemplate.find(new Query(Criteria.where("userId").in(ids)), UbMobileDeviceDoc.class);
    }

    public UbMobileDeviceDoc findByDeviceToken(String deviceToken, DeviceType deviceType) {
        return mongoTemplate.findOne(
                new Query(
                        Criteria.where("deviceToken").is(deviceToken)
                                .and("deviceType").is(deviceType)),
                UbMobileDeviceDoc.class);
    }

    private UbMobileDeviceDoc save(UbMobileDeviceDoc ubMobileDeviceDoc) {
        ubMobileDeviceDoc.setUpdateDate(new Date());
        mongoTemplate.save(ubMobileDeviceDoc);
        callAfterSave(ubMobileDeviceDoc);
        return ubMobileDeviceDoc;
    }

    public UbMobileDeviceDoc findById(ObjectId id) {
        return mongoTemplate.findById(id, UbMobileDeviceDoc.class);
    }


    public void remove(ObjectId id) {
        UbMobileDeviceDoc ubMobileDeviceDoc = findById(id);
        if (ubMobileDeviceDoc == null) return;
        mongoTemplate.remove(ubMobileDeviceDoc);
        callAfterDelete(ubMobileDeviceDoc);
    }

    public SearchUbMobileDeviceAdminResponse findAll(SearchUbMobileDeviceAdminRequest searchUbMobileDeviceAdminRequest) {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(
                searchUbMobileDeviceAdminRequest.getCurrentPage(),
                searchUbMobileDeviceAdminRequest.getPageSize(),
                sort);

        Criteria criteria = new Criteria();
        //Criteria.where("title").regex(searchCompanyAdminRequest.getQuery(), "i");

        Query query = new Query(criteria);
        Long count = mongoTemplate.count(query, UbMobileDeviceDoc.class);
        query = query.with(pageable);

        List<UbMobileDeviceDoc> result = mongoTemplate.find(query, UbMobileDeviceDoc.class);
        SearchUbMobileDeviceAdminResponse searchUbMobileDeviceAdminResponse = new SearchUbMobileDeviceAdminResponse(
                searchUbMobileDeviceAdminRequest.getCurrentPage(),
                searchUbMobileDeviceAdminRequest.getPageSize(),
                result);
        searchUbMobileDeviceAdminResponse.setAll(count);
        searchUbMobileDeviceAdminResponse.setQuery(searchUbMobileDeviceAdminRequest.getQuery());
        return searchUbMobileDeviceAdminResponse;
    }

    /**
     * Find all android devices
     *
     * @return
     */
    public List<UbMobileDeviceDoc> findAllAndroidDevices(UbMobileDeviceDoc... excludeDevices) {
        Criteria criteria = new Criteria("deviceType").is(DeviceType.ANDROID);

        if (excludeDevices != null && excludeDevices.length > 0) {
            criteria.and("_id").nin(excludeDevices);
        }

        Query query = new Query(criteria);

        return mongoTemplate.find(query, UbMobileDeviceDoc.class);
    }

    /**
     * Find all ios devices
     *
     * @return
     */
    public List<UbMobileDeviceDoc> findAllIosDevices() {
        Criteria criteria = new Criteria("deviceType").is(DeviceType.IOS);
        Query query = new Query(criteria);

        return mongoTemplate.find(query, UbMobileDeviceDoc.class);
    }

    private void callAfterSave(UbMobileDeviceDoc ubMobileDeviceDoc) {
        for (IUbMobileDeviceEvent iUbMobileDeviceEvent : ubMobileDeviceEvents.values()) {
            iUbMobileDeviceEvent.afterSave(ubMobileDeviceDoc);
        }
    }

    private void callAfterDelete(UbMobileDeviceDoc ubMobileDeviceDoc) {
        for (IUbMobileDeviceEvent iUbMobileDeviceEvent : ubMobileDeviceEvents.values()) {
            iUbMobileDeviceEvent.afterDelete(ubMobileDeviceDoc);
        }
    }
}
