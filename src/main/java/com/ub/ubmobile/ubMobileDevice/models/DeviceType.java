package com.ub.ubmobile.ubMobileDevice.models;

/**
 * Created by maslov on 06.11.15.
 */
public enum DeviceType {
    ANDROID, IOS
}
