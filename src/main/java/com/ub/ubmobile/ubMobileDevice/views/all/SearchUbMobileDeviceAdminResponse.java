package com.ub.ubmobile.ubMobileDevice.views.all;

import com.ub.core.base.search.SearchResponse;
import com.ub.ubmobile.ubMobileDevice.models.UbMobileDeviceDoc;

import java.util.List;

public class SearchUbMobileDeviceAdminResponse extends SearchResponse {
    private List<UbMobileDeviceDoc> result;


    public SearchUbMobileDeviceAdminResponse() {
    }

    public SearchUbMobileDeviceAdminResponse(Integer currentPage, Integer pageSize, List<UbMobileDeviceDoc> result) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.result = result;
    }

    public List<UbMobileDeviceDoc> getResult() {
        return result;
    }

    public void setResult(List<UbMobileDeviceDoc> result) {
        this.result = result;
    }
}
