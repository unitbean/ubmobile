package com.ub.ubmobile.ubMobileDevice.views.all;

import com.ub.core.base.search.SearchRequest;

public class SearchUbMobileDeviceAdminRequest extends SearchRequest{
    public SearchUbMobileDeviceAdminRequest() {
    }

    public SearchUbMobileDeviceAdminRequest(Integer currentPage){
        this.currentPage = currentPage;
    }

    public SearchUbMobileDeviceAdminRequest(Integer currentPage, Integer pageSize){
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

}
