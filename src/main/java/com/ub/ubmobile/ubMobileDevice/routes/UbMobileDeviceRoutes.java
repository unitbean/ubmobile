package com.ub.ubmobile.ubMobileDevice.routes;

import com.ub.core.base.routes.BaseRoutes;

public class UbMobileDeviceRoutes {
    private static final String ROOT = "/ubMobileDevice";
    public static final String REGISTER = ROOT + "/register";
}
