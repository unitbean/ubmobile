package com.ub.ubmobile.ubMobileDevice.controllers;

import com.ub.ubmobile.ubMobileDevice.models.DeviceType;
import com.ub.ubmobile.ubMobileDevice.routes.UbMobileDeviceRoutes;
import com.ub.ubmobile.ubMobileDevice.services.UbMobileDeviceService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UbMobileDeviceClientController {

    @Autowired private UbMobileDeviceService ubMobileDeviceService;

    @ResponseBody
    @RequestMapping(value = UbMobileDeviceRoutes.REGISTER, method = RequestMethod.POST, produces = "application/json")
    public Response registerDevice(@RequestParam String uuid, @RequestParam DeviceType deviceType,
                                   @RequestParam(required = false) String userId) {
        if (userId != null && !userId.isEmpty()) {
            ubMobileDeviceService.create(uuid, deviceType, new ObjectId(userId));
        } else {
            ubMobileDeviceService.create(uuid, deviceType);
        }

        return new Response();
    }

    private class Response {
        private boolean result = true;

        public boolean getResult() {
            return result;
        }

        public void setResult(boolean result) {
            this.result = result;
        }
    }
}
