package com.ub.ubmobile.ubMobileDevice.controllers;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.base.views.breadcrumbs.BreadcrumbsLink;
import com.ub.core.base.views.pageHeader.PageHeader;
import com.ub.ubmobile.ubMobileDevice.routes.UbMobileDeviceAdminRoutes;
import com.ub.ubmobile.ubMobileDevice.services.UbMobileDeviceService;
import com.ub.ubmobile.ubMobileDevice.views.all.SearchUbMobileDeviceAdminRequest;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UbMobileDeviceAdminController {
    @Autowired private UbMobileDeviceService ubMobileDeviceService;

    private PageHeader defaultPageHeader(String current){
        PageHeader pageHeader = PageHeader.defaultPageHeader();
        pageHeader.setLinkAdd(UbMobileDeviceAdminRoutes.ADD);
        pageHeader.getBreadcrumbs().getLinks().add(new BreadcrumbsLink(UbMobileDeviceAdminRoutes.ALL,"Все Mobile Device"));
        pageHeader.getBreadcrumbs().setCurrentPageTitle(current);
        return pageHeader;
    }



    @RequestMapping(value = UbMobileDeviceAdminRoutes.ALL, method = RequestMethod.GET)
    public String all(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                      @RequestParam(required = false, defaultValue = "") String query,
                      Model model) {
        SearchUbMobileDeviceAdminRequest searchUbMobileDeviceAdminRequest = new SearchUbMobileDeviceAdminRequest(currentPage);
        searchUbMobileDeviceAdminRequest.setQuery(query);
        model.addAttribute("searchUbMobileDeviceAdminResponse", ubMobileDeviceService.findAll(searchUbMobileDeviceAdminRequest));
        model.addAttribute("pageHeader",defaultPageHeader("Все"));
        return "com.ub.ubmobile.admin.ubMobileDevice.all";
    }

    @RequestMapping(value = UbMobileDeviceAdminRoutes.MODAL_PARENT, method = RequestMethod.GET)
    public String modalResponse(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                @RequestParam(required = false, defaultValue = "") String query,
                                Model model) {
        SearchUbMobileDeviceAdminRequest searchUbMobileDeviceAdminRequest = new SearchUbMobileDeviceAdminRequest(currentPage);
        searchUbMobileDeviceAdminRequest.setQuery(query);
        model.addAttribute("searchUbMobileDeviceAdminResponse", ubMobileDeviceService.findAll(searchUbMobileDeviceAdminRequest));
        return "com.ub.ubmobile.admin.ubMobileDevice.modal.parent";
    }

    @RequestMapping(value = UbMobileDeviceAdminRoutes.DELETE, method = RequestMethod.GET)
    public String delete(@RequestParam ObjectId id, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("pageHeader",defaultPageHeader("Удаление"));
        return "com.ub.ubmobile.admin.ubMobileDevice.delete";
    }

    @RequestMapping(value = UbMobileDeviceAdminRoutes.DELETE, method = RequestMethod.POST)
    public String delete(@RequestParam ObjectId id) {
        ubMobileDeviceService.remove(id);
        return RouteUtils.redirectTo(UbMobileDeviceAdminRoutes.ALL);
    }
}
