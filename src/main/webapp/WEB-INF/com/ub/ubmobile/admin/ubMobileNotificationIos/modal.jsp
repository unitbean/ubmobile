<%@ page import="com.ub.ubmobile.ubMobileNotificationIos.routes.UbMobileDeviceNotificationIosAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="modal fade custom-width" id="modal-ubMobileNotificationIos">
    <div class="modal-dialog" style="width: 96%">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбор Notification Ios</h4>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-lg-5">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="modal-ubMobileNotificationIos-query" name="query"
                                   value="" placeholder="Поиск"/>

                            <div class="input-group-btn">
                                <button type="submit" id="modal-ubMobileNotificationIos-search" class="btn btn-sm btn-default"><i
                                        class="entypo-search">Поиск </i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <section id="modal-ubMobileNotificationIos-parent-content"></section>


            </div>

            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<script>
    function initUbMobileDeviceNotificationIosModal() {
        $.get("<%= UbMobileDeviceNotificationIosAdminRoutes.MODAL_PARENT%>",
                { query: $('#modal-ubMobileNotificationIos-query').val() },
                function (data) {
                    updateUbMobileDeviceNotificationIosContent(data);
                });
    }

    function updateUbMobileDeviceNotificationIosContent(data) {
        $('#modal-ubMobileNotificationIos-parent-content').html(data);
    }

    function onClickUbMobileDeviceNotificationIosMTable() {
        var id = $(this).attr('data-id');
        var title = $(this).attr('data-title');
        $('#parent-ubMobileNotificationIos-hidden').val(id);
        $('#parent-ubMobileNotificationIos').val(title);
        $('#modal-ubMobileNotificationIos').modal('hide');
    }

    function onClickUbMobileDeviceNotificationIosPaginator() {
        var q = $(this).attr('data-query');
        var n = $(this).attr('data-number');
        $.get("<%= UbMobileDeviceNotificationIosAdminRoutes.MODAL_PARENT%>",
                { query: q, currentPage: n},
                function (data) {
                    updateUbMobileDeviceNotificationIosContent(data);
                });
    }

    $(function () {
        $('#btn_parent_ubMobileNotificationIos').click(function () {
            $('#modal-ubMobileNotificationIos').modal('show');
            initUbMobileDeviceNotificationIosModal();
            return false;
        });
        $('#btn_parent_ubMobileNotificationIos_clear').click(function () {
            $('#parent-ubMobileNotificationIos-hidden').val('');
            $('#parent-ubMobileNotificationIos').val('');
            return false;
        });

        $('#modal-ubMobileNotificationIos').on('click', '.modal-ubMobileNotificationIos-line', onClickUbMobileDeviceNotificationIosMTable);
        $('#modal-ubMobileNotificationIos').on('click', '.modal-ubMobileNotificationIos-goto', onClickUbMobileDeviceNotificationIosPaginator);
        $('#modal-ubMobileNotificationIos-search').click(initUbMobileDeviceNotificationIosModal);

    });
</script>
