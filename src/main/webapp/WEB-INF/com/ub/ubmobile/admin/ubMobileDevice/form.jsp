<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form:errors path="*" cssClass="alert alert-warning" element="div" />
<form:hidden path="id"/>


<div class="row">
    <div class="col-lg-12">
        <label for="deviceToken">Device token</label>
        <form:input path="deviceToken" cssClass="form-control" id="deviceToken"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="deviceType">Device type</label>
        <form:input path="deviceType" cssClass="form-control" id="deviceType"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="createDate">Create date</label>
        <form:input path="createDate" cssClass="form-control" id="createDate"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="updateDate">Update date</label>
        <form:input path="updateDate" cssClass="form-control" id="updateDate"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="userId">User id</label>
        <form:input path="userId" cssClass="form-control" id="userId"/>
    </div>
</div>

