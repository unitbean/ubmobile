<%@ page import="com.ub.ubmobile.ubMobileNotificationAndroid.routes.UbMobileDeviceNotificationAndroidAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="modal fade custom-width" id="modal-ubMobileNotificationAndroid">
    <div class="modal-dialog" style="width: 96%">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбор Notification Android</h4>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-lg-5">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="modal-ubMobileNotificationAndroid-query" name="query"
                                   value="" placeholder="Поиск"/>

                            <div class="input-group-btn">
                                <button type="submit" id="modal-ubMobileNotificationAndroid-search" class="btn btn-sm btn-default"><i
                                        class="entypo-search">Поиск </i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <section id="modal-ubMobileNotificationAndroid-parent-content"></section>


            </div>

            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<script>
    function initUbMobileDeviceNotificationAndroidModal() {
        $.get("<%= UbMobileDeviceNotificationAndroidAdminRoutes.MODAL_PARENT%>",
                { query: $('#modal-ubMobileNotificationAndroid-query').val() },
                function (data) {
                    updateUbMobileDeviceNotificationAndroidContent(data);
                });
    }

    function updateUbMobileDeviceNotificationAndroidContent(data) {
        $('#modal-ubMobileNotificationAndroid-parent-content').html(data);
    }

    function onClickUbMobileDeviceNotificationAndroidMTable() {
        var id = $(this).attr('data-id');
        var title = $(this).attr('data-title');
        $('#parent-ubMobileNotificationAndroid-hidden').val(id);
        $('#parent-ubMobileNotificationAndroid').val(title);
        $('#modal-ubMobileNotificationAndroid').modal('hide');
    }

    function onClickUbMobileDeviceNotificationAndroidPaginator() {
        var q = $(this).attr('data-query');
        var n = $(this).attr('data-number');
        $.get("<%= UbMobileDeviceNotificationAndroidAdminRoutes.MODAL_PARENT%>",
                { query: q, currentPage: n},
                function (data) {
                    updateUbMobileDeviceNotificationAndroidContent(data);
                });
    }

    $(function () {
        $('#btn_parent_ubMobileNotificationAndroid').click(function () {
            $('#modal-ubMobileNotificationAndroid').modal('show');
            initUbMobileDeviceNotificationAndroidModal();
            return false;
        });
        $('#btn_parent_ubMobileNotificationAndroid_clear').click(function () {
            $('#parent-ubMobileNotificationAndroid-hidden').val('');
            $('#parent-ubMobileNotificationAndroid').val('');
            return false;
        });

        $('#modal-ubMobileNotificationAndroid').on('click', '.modal-ubMobileNotificationAndroid-line', onClickUbMobileDeviceNotificationAndroidMTable);
        $('#modal-ubMobileNotificationAndroid').on('click', '.modal-ubMobileNotificationAndroid-goto', onClickUbMobileDeviceNotificationAndroidPaginator);
        $('#modal-ubMobileNotificationAndroid-search').click(initUbMobileDeviceNotificationAndroidModal);

    });
</script>
