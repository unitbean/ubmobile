<%@ page import="com.ub.ubmobile.ubMobileDevice.routes.UbMobileDeviceAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="modal fade custom-width" id="modal-ubMobileDevice">
    <div class="modal-dialog" style="width: 96%">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбор Mobile Device</h4>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-lg-5">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="modal-ubMobileDevice-query" name="query"
                                   value="" placeholder="Поиск"/>

                            <div class="input-group-btn">
                                <button type="submit" id="modal-ubMobileDevice-search" class="btn btn-sm btn-default"><i
                                        class="entypo-search">Поиск </i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <section id="modal-ubMobileDevice-parent-content"></section>


            </div>

            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<script>
    function initUbMobileDeviceModal() {
        $.get("<%= UbMobileDeviceAdminRoutes.MODAL_PARENT%>",
                { query: $('#modal-ubMobileDevice-query').val() },
                function (data) {
                    updateUbMobileDeviceContent(data);
                });
    }

    function updateUbMobileDeviceContent(data) {
        $('#modal-ubMobileDevice-parent-content').html(data);
    }

    function onClickUbMobileDeviceMTable() {
        var id = $(this).attr('data-id');
        var title = $(this).attr('data-title');
        $('#parent-ubMobileDevice-hidden').val(id);
        $('#parent-ubMobileDevice').val(title);
        $('#modal-ubMobileDevice').modal('hide');
    }

    function onClickUbMobileDevicePaginator() {
        var q = $(this).attr('data-query');
        var n = $(this).attr('data-number');
        $.get("<%= UbMobileDeviceAdminRoutes.MODAL_PARENT%>",
                { query: q, currentPage: n},
                function (data) {
                    updateUbMobileDeviceContent(data);
                });
    }

    $(function () {
        $('#btn_parent_ubMobileDevice').click(function () {
            $('#modal-ubMobileDevice').modal('show');
            initUbMobileDeviceModal();
            return false;
        });
        $('#btn_parent_ubMobileDevice_clear').click(function () {
            $('#parent-ubMobileDevice-hidden').val('');
            $('#parent-ubMobileDevice').val('');
            return false;
        });

        $('#modal-ubMobileDevice').on('click', '.modal-ubMobileDevice-line', onClickUbMobileDeviceMTable);
        $('#modal-ubMobileDevice').on('click', '.modal-ubMobileDevice-goto', onClickUbMobileDevicePaginator);
        $('#modal-ubMobileDevice-search').click(initUbMobileDeviceModal);

    });
</script>
