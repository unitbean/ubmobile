<%@ page import="com.ub.ubmobile.ubMobileDevice.routes.UbMobileDeviceAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<style>
    .curcor-pointer:hover {
        cursor: pointer;
        background-color: whitesmoke;
    }
</style>
<div class="row" style="margin-top: 10px">
    <div class="col-lg-12">
        <table class="table table-bordered" id="table-1">
            <thead>
            <tr>
               <th>Device token</th><th>Device type</th><th>Create date</th><th>Update date</th><th>User id</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${searchUbMobileDeviceAdminResponse.result}" var="doc">

                <tr class="curcor-pointer modal-ubMobileDevice-line" data-id="${doc.id}" data-title="${doc.title}">
                   <td>${doc.deviceToken}</td><td>${doc.deviceType}</td><td>${doc.createDate}</td><td>${doc.updateDate}</td><td>${doc.userId}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        <ul class="pagination pagination-sm">

            <li>
                <a href="#" class="modal-ubMobileDevice-goto" data-query="${searchUbMobileDeviceAdminResponse.query}"
                   data-number="${searchUbMobileDeviceAdminResponse.prevNum()}">
                <i class="entypo-left-open-mini"></i></a>
            </li>
            <c:forEach items="${searchUbMobileDeviceAdminResponse.paginator()}" var="page">
                <li class="<c:if test="${searchUbMobileDeviceAdminResponse.currentPage eq page}">active</c:if>">
                    <a href="#" class="modal-ubMobileDevice-goto" data-query="${searchUbMobileDeviceAdminResponse.query}"
                       data-number="${page}">${page + 1}</a>
                </li>
            </c:forEach>
            <li>
                <a href="#" class="modal-ubMobileDevice-goto" data-query="${searchUbMobileDeviceAdminResponse.query}"
                   data-number="${searchUbMobileDeviceAdminResponse.nextNum()}"><i class="entypo-right-open-mini"></i></a>
            </li>
        </ul>
    </div>
</div>