<%@ page import="com.ub.ubmobile.ubMobileNotificationAndroid.models.AndroidPriorityEnum" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form:errors path="*" cssClass="alert alert-warning" element="div" />
<form:hidden path="id"/>

<div class="row">
    <div class="col-md-12">
        <label for="gcmRegId">Gcm reg id</label>
        <form:input path="gcmRegId" cssClass="form-control" id="gcmRegId"/>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="apiKey">Api key</label>
        <form:input path="apiKey" cssClass="form-control" id="apiKey"/>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label for="priority">Priority</label>
        <select name="priority" class="form-control" id="priority">
            <c:forEach items="<%=AndroidPriorityEnum.values()%>" var="type">
                <option value="${type}" <c:if test="${ubMobileNotificationAndroidDoc.priority eq type}">selected</c:if>>${type.title}</option>
            </c:forEach>
        </select>
    </div>

    <div class="col-md-6">
        <label for="priority">Delay while idle</label>
        <input type="checkbox" name="delayWhileIdle" <c:if test="${ubMobileNotificationAndroidDoc.delayWhileIdle}">checked</c:if>/>
    </div>
</div>
