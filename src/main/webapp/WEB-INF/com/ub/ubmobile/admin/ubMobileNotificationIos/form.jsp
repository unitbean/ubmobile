<%@ page import="com.ub.ubmobile.ubMobileNotificationIos.models.IosNotificationType" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form:errors path="*" cssClass="alert alert-warning" element="div" />
<form:hidden path="id"/>

<div class="row">
    <div class="col-md-12">
        <label for="password">Password</label>
        <form:input path="password" cssClass="form-control" id="password"/>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="file">New certificate (Old certificate name: ${ubMobileNotificationIosDoc.fileName})</label>
        <input type="file" name="file" class="form-control" id="file"/>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="type">Type</label>
        <select name="type" id="type" class="form-control">
            <c:forEach items="<%=IosNotificationType.values()%>" var="type">
                <option <c:if test="${ubMobileNotificationIosDoc.type eq type}">selected</c:if> value="${type.name()}">${type.title}</option>
            </c:forEach>
        </select>
    </div>
</div>